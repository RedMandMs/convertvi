package com.trollfish.convertvi;

import java.io.IOException;

/**
 * Интерфейс сериализатора vi-формата (native) конкретной структуры ВИ
 * @param <T> - структура для сериализации
 */
public interface INativeSerializer<T> {

    /**
     * Сериализация структуры
     * @param object - структура ВИ
     * @param writer - писатель-VI
     */
    public void serialize(T object, NativeStreamWriter writer) throws IOException;

    /**
     * Десериализация структуры
     * @param reader - читатель-VI
     * @return - структуры в программном представлении
     */
    public T deserialize (Object addInformation, NativeStreamReader reader) throws IOException;

    public int size();

}
