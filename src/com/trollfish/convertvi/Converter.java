package com.trollfish.convertvi;

import com.trollfish.convertvi.instrument.Instrument;

import java.io.File;

/**
 * Класс преобразователя форматов
 */
public class Converter {

    /**
     * Десериализация vi-формата (native)
     * @param file - файл с ВИ
     * @return - программная реализация ВИ
     */
    protected Instrument deserializeVI(File file){

        return null;
    }

    /**
     * Десериализация json-формата (customize)
     * @param file - файл с ВИ
     * @return - программная реализация ВИ
     */
    protected Instrument deserializeJSON(File file){

        return null;
    }

    /**
     * Сериализация в vi-формат (native)
     * @param dir - директория сохранения файла
     */
    protected void serializeToVI(File dir){

    }

    /**
     * Сериализация в json-формат (customize)
     * @param dir - директория сохранения файла
     */
    protected void serializeToJSON(File dir){

    }

}
