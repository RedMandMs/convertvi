package com.trollfish.convertvi;

/**
 * Интерфейс сериализуемой структуры для преобразования в vi-формат
 */
public interface INativeSerializable {

    /**
     * Получение экземпляра vi-сериализатора (Singleton)
     * @return - экземпляр vi-сериализатора
     */
    public INativeSerializer getNativeSerializer();

    /**
     * Получение размера структуры в байтах
     * @return - размер (в байтах)
     */
    public int getSize();
}
