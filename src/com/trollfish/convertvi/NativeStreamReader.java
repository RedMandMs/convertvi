package com.trollfish.convertvi;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Vi-читатель
 */
public class NativeStreamReader {

    /**
     * Консструктор с входным массивом байтов
     * @param raw - массив байтов обрабатываемого vi-файла
     */
    public NativeStreamReader(byte[] raw) {
        this.stream = new ByteArrayInputStream(raw);
        this.bytes = null;
        this.size = raw.length;
    }

    /**
     * Считывание байт-кода (определённого числа байтов)
     * @param count - колличество считываемых байтов
     * @return - буффер с байтами
     * @throws IOException
     */
    public ByteBuffer holdBytes(int count) throws IOException {
        if(bytes != null){

            throw new IOException("bytes != null");
        }

        bytes = new byte[count];

        if(stream.read(bytes) != count){

            System.out.println("read= " + stream.read(bytes) + ";  count = " + count);

            throw new IOException("stream.read(bytes) != count");

        }

        return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN);
    }

    /**
     * Сброс массива байтов
     */
    public void releaseBytes() {
        bytes = null;
    }

    /**
     * Перемещение указателя по массиву байтов
     * @param position - новая позиция
     */
    public void seek(long position){
        stream.reset();
        stream.skip(position);
    }

    /**
     * определение оставшихся за указателем байтов
     * @return - колличество оставшихся байтов
     */
    public int residue(){
        int rest = (int) stream.skip(size);
        seek(size - rest);
        return rest;
    }

    private ByteArrayInputStream stream;            //поток для считывания байтов
    private byte[] bytes;                           //обрабатываемый массив байтов
    private int size;                               //размер ВИ
}