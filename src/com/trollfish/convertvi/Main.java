package com.trollfish.convertvi;

import com.trollfish.convertvi.instrument.Instrument;
import com.trollfish.convertvi.instrument.InstrumentJSonSerializer;
import com.trollfish.convertvi.instrument.InstrumentNativeSerializer;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

/**
 * Created by Сергей on 14.04.14.
 */
public class Main {

    public static void main(String[] args){
        GUI gui = new GUI();

        //создание графического интерфейса
        gui.create();
    }

    /**
     * Тестовый запуск №2
     */
    private void goTest2() throws IOException{
/*
        JSONObject jSon = new JSONObject();
        JSONObject jSonIner = new JSONObject();
        jSonIner.put("Id1","k1");
        jSonIner.put("Id2","k2");
        jSon.put("GId1","g1");
        jSon.put("LId", jSonIner);


            FileWriter file = new FileWriter("D://test.json");
            file.write(jSon.toJSONString());
            file.flush();
            file.close();


        System.out.println(jSon);

*/
        JSONObject jSon;
        JSONParser parser = new JSONParser();
        Instrument instrument;
        try {
            Object obj = parser.parse(new FileReader("D://test.json"));
            jSon = (JSONObject) obj;
        } catch (ParseException e) {
            System.out.println("Ошибка!!!");
            e.printStackTrace();
            jSon = new JSONObject();

        }
        InstrumentJSonSerializer writer = InstrumentJSonSerializer.getInstance();
        instrument = writer.deserialize(jSon);
        System.out.println(instrument.getRsrc().getDataSet().getBlockList().get(0).getInformation());




    }

    /**
     * Тестовый запуск №1
     */
    public void goTest1() throws IOException {
        File myfile = new File("C://Users/Сергей/IdeaProjects/Сonvertvi/src/Test.vi");
        FileInputStream fileInputStream = new FileInputStream(myfile);

        byte [] bytes = new byte[(int)myfile.length()];
        fileInputStream.read(bytes);

        NativeStreamReader reader = new NativeStreamReader(bytes);
        InstrumentNativeSerializer serializer = InstrumentNativeSerializer.getInstance();

        Instrument instrument = serializer.deserialize(null, reader);

        File resultFile = new File("C://Users/Сергей/IdeaProjects/Сonvertvi/Result.vi");
        FileOutputStream fileOutputStream = new FileOutputStream(resultFile);
        NativeStreamWriter writer = new NativeStreamWriter();

        serializer.serialize(instrument, writer);

        writer.flushBytes(fileOutputStream);

        fileOutputStream.close();


    }
}
