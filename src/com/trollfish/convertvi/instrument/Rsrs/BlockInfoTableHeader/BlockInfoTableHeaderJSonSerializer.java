package com.trollfish.convertvi.instrument.Rsrs.BlockInfoTableHeader;

import com.trollfish.convertvi.IJSonSerializer;
import org.json.simple.JSONObject;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * Json-сериализатор структуры BlockInfoTableHeader
 */
public class BlockInfoTableHeaderJSonSerializer implements IJSonSerializer<BlockInfoTableHeader> {

    @Override
    public void serialize(BlockInfoTableHeader object, JSONObject writer) {

    }

    @Override
    public BlockInfoTableHeader deserialize(JSONObject reader) {
        return null;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static BlockInfoTableHeaderJSonSerializer getInstance() {
        if (instance == null)
            instance = new BlockInfoTableHeaderJSonSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private BlockInfoTableHeaderJSonSerializer() {

    }

    private static BlockInfoTableHeaderJSonSerializer instance = null;          //экземпляр данного класса (Singleton)

}