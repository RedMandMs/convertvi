package com.trollfish.convertvi.instrument.Rsrs.BlockInfoTableHeader;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * vi--сериализатор структуры BlockInfoTableHeader
 */
public class BlockInfoTableHeaderNativeSerializer implements INativeSerializer<BlockInfoTableHeader> {

    @Override
    public void serialize(BlockInfoTableHeader blockInfoTableHeader, NativeStreamWriter writer) throws IOException {
        ByteBuffer bb = writer.holdBytes(blockInfoTableHeader.getSize());

        bb.putInt(blockInfoTableHeader.offset);
        bb.putInt(blockInfoTableHeader.varSize);
        bb.putInt(blockInfoTableHeader.count);

        writer.releaseBytes();
    }

    @Override
    public BlockInfoTableHeader deserialize(Object addInformation, NativeStreamReader reader) throws IOException {
        ByteBuffer bb = reader.holdBytes(size());

        int offset = bb.getInt();

        int varSize = bb.getInt();

        int count = bb.getInt();

        BlockInfoTableHeader object = new BlockInfoTableHeader(
                size(), offset, varSize, count
        );

        reader.releaseBytes();

        return object;
    }

    @Override
    public int size() {
        return BLOCK_INFO_TABLE_HEDER_SIZE; // BlockInfoTableHeader object has constant size
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static BlockInfoTableHeaderNativeSerializer getInstance() {
        if (instance == null)
            instance = new BlockInfoTableHeaderNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private BlockInfoTableHeaderNativeSerializer() {

    }

    private static final int BLOCK_INFO_TABLE_HEDER_SIZE = 12;              //размер (в байтах) данной структуры (всегда фиксированный)

    private static BlockInfoTableHeaderNativeSerializer instance = null;    //экземпляр данного класса (Singleton)
}
