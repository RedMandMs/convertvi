package com.trollfish.convertvi.instrument.Rsrs.BlockInfoTableHeader;

import com.trollfish.convertvi.IJSonSerializable;
import com.trollfish.convertvi.IJSonSerializer;
import com.trollfish.convertvi.INativeSerializable;
import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.Rsrs;

/**
 * Программное представление структуры BlockInfoTableHeader
 */
public class BlockInfoTableHeader implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return BlockInfoTableHeaderNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return BlockInfoTableHeaderJSonSerializer.getInstance();
    }

    /**
     * Конструктор
     * @param size - размер структуры
     * @param offset
     * @param varSize
     * @param count
     */
    protected BlockInfoTableHeader(int size, int offset, int varSize, int count){
        this.size = size;
        this.offset = offset;
        this.varSize = varSize;
        this.count = count;
    }

    protected Rsrs root;        //структура-родитель
    protected int offset;
    protected int varSize;
    private int size;           //размер структуры

    /**
     * Гетеры и сетеры
     */
    public int getSize() {
        return size;
    }
    public int getCount() {
        return count;
    }
    protected int count;
    public Rsrs getRoot(){
        return root;
    }
    public void setRoot(Rsrs root){
        this.root = root;
    }
}
