package com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable;

import com.trollfish.convertvi.IJSonSerializable;
import com.trollfish.convertvi.IJSonSerializer;
import com.trollfish.convertvi.INativeSerializable;
import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable.Block.Block;
import com.trollfish.convertvi.instrument.Rsrs.Rsrs;

import java.util.ArrayList;

/**
 * Программное представление структуры BlockInfoDataTable
 */
public class BlockInfoDataTable implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return BlockInfoDataTableNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return BlockInfoDataTableJSonSerializer.getInstance();
    }

    /**
     * Конструктор
     * @param size - размер структуры (в байтах)
     * @param blockList
     */
    protected BlockInfoDataTable(int size, ArrayList<Block> blockList){
        this.size = size;
        this.blockList = blockList;
        for(int i=0; i<blockList.size(); i++){
            blockList.get(i).setRoot(this);
        }
    }

    private int size;                                             //размер структуры (в байтах)
    protected Rsrs root;                                          //структура-родитель
    protected ArrayList<Block> blockList = new ArrayList<Block>();//список блоков

    /**
     * Гетеры и сетеры
     */
    public int getSize() {
        return size;
    }
    public ArrayList<Block> getBlockList() {
        return blockList;
    }
    public Rsrs getRoot(){
        return root;
    }
    public void setRoot(Rsrs root){
        this.root = root;
    }
}
