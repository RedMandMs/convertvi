package com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable;

import com.trollfish.convertvi.IJSonSerializer;
import org.json.simple.JSONObject;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * Json-сериализатор структуры BlockInfoDataTable
 */
public class BlockInfoDataTableJSonSerializer implements IJSonSerializer<BlockInfoDataTable> {

    @Override
    public void serialize(BlockInfoDataTable object, JSONObject writer) {

    }

    @Override
    public BlockInfoDataTable deserialize(JSONObject reader) {
        return null;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static BlockInfoDataTableJSonSerializer getInstance() {
        if (instance == null)
            instance = new BlockInfoDataTableJSonSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private BlockInfoDataTableJSonSerializer() {

    }

    private static BlockInfoDataTableJSonSerializer instance = null;        //экземпляр данного класса (Singleton)

}
