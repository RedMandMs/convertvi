package com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable.Block;

import com.trollfish.convertvi.IJSonSerializable;
import com.trollfish.convertvi.IJSonSerializer;
import com.trollfish.convertvi.INativeSerializable;
import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable.BlockInfoDataTable;

/**
 * Программное представление структуры Block
 */
public class Block implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return BlockNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return BlockJSonSerializer.getInstance();
    }

    /**
     * Конструктор
     * @param size - размер структуры (в байтах)
     * @param id1
     * @param id2
     * @param id3
     * @param offset
     * @param id4
     * @param name
     */
    protected Block(int size, int id1, int id2, int id3, int offset, int id4, String name){
        this.size = size;
        this.id1 = id1;
        this.id2 = id2;
        this.id3 = id3;
        this.offset = offset;
        this.id4 = id4;
        this.name = name;
    }

    protected BlockInfoDataTable root;              //структура-родитель
    protected int id1;
    protected int id2;
    protected int id3;
    protected int offset;
    protected int id4;
    private int size;                              //размер структуры (в байтах)
    protected String name;

    /**
     * Гетеры и сетеры
     */
    public int getSize() {
        return size;
    }
    public String getName() {
        return name;
    }
    public BlockInfoDataTable getRoot(){
        return root;
    }
    public void setRoot(BlockInfoDataTable root){
        this.root = root;
    }
}
