package com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable.Block;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * VI-сериализатор структуры Block
 */
public class BlockNativeSerializer implements INativeSerializer<Block> {

    @Override
    public void serialize(Block block, NativeStreamWriter writer) throws IOException {
        ByteBuffer bb = writer.holdBytes(block.getSize());

        bb.putInt(block.id1);

        bb.putInt(block.id2);

        bb.putInt(block.id3);

        bb.putInt(block.offset);

        bb.putInt(block.id4);

        writer.releaseBytes();
    }

    @Override
    public Block deserialize(Object name, NativeStreamReader reader) throws IOException {
        ByteBuffer bb = reader.holdBytes(size());

        int id1 = bb.getInt();

        int id2 = bb.getInt();

        int id3 = bb.getInt();

        int offset = bb.getInt();

        int id4 = bb.getInt();

        String thisName = (String) name;

        Block object = new Block(
                size(), id1, id2, id3, offset, id4, thisName
        );

        reader.releaseBytes();

        return object;
    }

    @Override
    public int size() {
        return BLOCK_SIZE; // Block object has constant size
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static BlockNativeSerializer getInstance() {
        if (instance == null)
            instance = new BlockNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private BlockNativeSerializer() {

    }

    private static final int BLOCK_SIZE = 20;                   //размер (в байтах) данной структуры

    private static BlockNativeSerializer instance = null;       //экземпляр данного класса (Singleton)

}
