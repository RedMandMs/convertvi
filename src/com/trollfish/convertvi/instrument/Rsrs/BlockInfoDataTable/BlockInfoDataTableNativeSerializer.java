package com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable.Block.Block;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable.Block.BlockNativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable.BlockInfoHeaderTable;

import java.io.IOException;
import java.util.ArrayList;

/**
 * VI-сериализатор структуры BlockInfoDataTable
 */
public class BlockInfoDataTableNativeSerializer implements INativeSerializer<BlockInfoDataTable> {

    @Override
    public void serialize(BlockInfoDataTable blockInfoDataTable, NativeStreamWriter writer) throws IOException {

        for(int i=0; i<blockInfoDataTable.blockList.size(); i++){
            blockInfoDataTable.blockList.get(i).getNativeSerializer().serialize(blockInfoDataTable.blockList.get(i), writer);
        }

    }

    @Override
    public BlockInfoDataTable deserialize(Object addInformation, NativeStreamReader reader) throws IOException {


        ArrayList<Block> blockList = new ArrayList<Block>();

        BlockNativeSerializer serializerBlock = BlockNativeSerializer.getInstance();

        BlockInfoHeaderTable blockInfoHeaderTable = (BlockInfoHeaderTable) addInformation;

        int countBlocks = 0;
        ArrayList<String> namesBlockList = new ArrayList<String>();
        for(int i=0; i<blockInfoHeaderTable.getBlockList().size(); i++){
            countBlocks = countBlocks + blockInfoHeaderTable.getBlockList().get(i).getCount() + 1;
            for (int j = 0; j < blockInfoHeaderTable.getBlockList().get(i).getCount() + 1; j++) {
                namesBlockList.add(blockInfoHeaderTable.getBlockList().get(i).getName());
            }
        }

        for (int i=0; i<countBlocks; i++){
            String name = namesBlockList.get(i);

            blockList.add(serializerBlock.deserialize(name, reader));
        }

        this.Block_Info_Data_Table_Size = blockList.size() * 20;

        BlockInfoDataTable blockInfoDataTable = new BlockInfoDataTable(size(), blockList);

        reader.releaseBytes();

        return blockInfoDataTable;
    }

    @Override
    public int size() {
        return Block_Info_Data_Table_Size;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static BlockInfoDataTableNativeSerializer getInstance() {
        if (instance == null)
            instance = new BlockInfoDataTableNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private BlockInfoDataTableNativeSerializer() {

    }

    private int Block_Info_Data_Table_Size;                 //размер (в байтах) данной структуры

    private static BlockInfoDataTableNativeSerializer instance = null;      //экземпляр данного класса (Singleton)
}