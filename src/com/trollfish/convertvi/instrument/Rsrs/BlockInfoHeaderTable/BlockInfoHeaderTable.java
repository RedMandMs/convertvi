package com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable;

import com.trollfish.convertvi.IJSonSerializable;
import com.trollfish.convertvi.IJSonSerializer;
import com.trollfish.convertvi.INativeSerializable;
import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable.Block.Block;
import com.trollfish.convertvi.instrument.Rsrs.Rsrs;

import java.util.ArrayList;

/**
 * Программное представление структуры BlockInfoHeaderTable
 */
public class BlockInfoHeaderTable implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
            return BlockInfoHeaderTableNativeSerializer.getInstance();
        }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return BlockInfoHeaderTableJSonSerializer.getInstance();
    }

    /**
     * Конструктор
     * @param size - размер структуры
     * @param blockList - список блоков
     */
    protected BlockInfoHeaderTable(int size, ArrayList<Block> blockList){
        this.size = size;
        this.blockList = blockList;
        for(int i=0; i<blockList.size(); i++){
            this.blockList.get(i).setRoot(this);
        }
    }

    private int size;                                               //размер структуры
    protected Rsrs root;                                            //структура-родитель
    protected ArrayList<Block> blockList = new ArrayList<Block>();      //список блоков

    /**
     * Гетеры и сетеры
     */
    public int getSize() {
        return size;
    }
    public ArrayList<Block> getBlockList() {
        return blockList;
    }
    public Rsrs getRoot(){
        return root;
    }
    public void setRoot(Rsrs root){
        this.root = root;
    }
}
