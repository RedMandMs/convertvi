package com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable.Block.Block;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable.Block.BlockNativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoTableHeader.BlockInfoTableHeader;

import java.io.IOException;
import java.util.ArrayList;

/**
 * VI-сериализатор структуры BlockInfoHeaderTable
 */
public class BlockInfoHeaderTableNativeSerializer implements INativeSerializer<BlockInfoHeaderTable> {

    @Override
    public void serialize(BlockInfoHeaderTable blockInfoHeaderTable, NativeStreamWriter writer) throws IOException {

        for (int i=0; i<blockInfoHeaderTable.blockList.size(); i++){
            blockInfoHeaderTable.blockList.get(i).getNativeSerializer().serialize(blockInfoHeaderTable.blockList.get(i), writer);
        }

    }

    @Override
    public BlockInfoHeaderTable deserialize(Object addInformation, NativeStreamReader reader) throws IOException {

        ArrayList<Block> blockList = new ArrayList<Block>();
        BlockNativeSerializer serializerBlock = BlockNativeSerializer.getInstance();

        BlockInfoTableHeader blockInfoTableHeader = (BlockInfoTableHeader) addInformation;

        int countHeaderBlocks = blockInfoTableHeader.getCount()+1;

        for (int i=0; i<countHeaderBlocks; i++){
            blockList.add(serializerBlock.deserialize(null, reader));
        }

        BLOCK_INFO_HEADER_TABLE_SIZE = blockList.size() * 12;

        BlockInfoHeaderTable blockIHT = new BlockInfoHeaderTable(
                size(), blockList
        );

        reader.releaseBytes();

        return blockIHT;
    }

    @Override
    public int size() {
        return BLOCK_INFO_HEADER_TABLE_SIZE;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static BlockInfoHeaderTableNativeSerializer getInstance() {
        if (instance == null)
            instance = new BlockInfoHeaderTableNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private BlockInfoHeaderTableNativeSerializer() {

    }

    private int BLOCK_INFO_HEADER_TABLE_SIZE;                                //размер (в байтах) данной структуры

    private static BlockInfoHeaderTableNativeSerializer instance = null;    //экземпляр данного класса (Singleton)
}