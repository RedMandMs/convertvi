package com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable;

import com.trollfish.convertvi.IJSonSerializer;
import org.json.simple.JSONObject;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * Json-сериализатор структуры BlockInfoHeaderTable
 */
public class BlockInfoHeaderTableJSonSerializer implements IJSonSerializer<BlockInfoHeaderTable> {

    @Override
    public void serialize(BlockInfoHeaderTable object, JSONObject writer) {

    }

    @Override
    public BlockInfoHeaderTable deserialize(JSONObject reader) {
        return null;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static BlockInfoHeaderTableJSonSerializer getInstance() {
        if (instance == null)
            instance = new BlockInfoHeaderTableJSonSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private BlockInfoHeaderTableJSonSerializer() {

    }

    private static BlockInfoHeaderTableJSonSerializer instance = null;          //экземпляр данного класса (Singleton)

}