package com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable.Block;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * VI-сериализатор структуры Block
 */
public class BlockNativeSerializer implements INativeSerializer<Block> {

    @Override
    public void serialize(Block block, NativeStreamWriter writer) throws IOException {
        ByteBuffer bb = writer.holdBytes(block.getSize());

        bb.put(block.name.getBytes());

        bb.putInt(block.count);

        bb.putInt(block.offset);

        writer.releaseBytes();
    }

    @Override
    public Block deserialize(Object addInformation, NativeStreamReader reader) throws IOException {
        ByteBuffer bb = reader.holdBytes(size());

        byte[] name = new byte[4];
        bb.get(name);

        int count = bb.getInt();

        int offset = bb.getInt();

        Block object = new Block(
                size(), new String(name), count, offset
        );

        reader.releaseBytes();

        return object;
    }

    @Override
    public int size() {
        return BLOCK_SIZE;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static BlockNativeSerializer getInstance() {
        if (instance == null)
            instance = new BlockNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private BlockNativeSerializer() {

    }


    private static final int BLOCK_SIZE = 12;  //размер (в байтах) данной структуры

    private static BlockNativeSerializer instance = null;       //экземпляр данного класса (Singleton)

}