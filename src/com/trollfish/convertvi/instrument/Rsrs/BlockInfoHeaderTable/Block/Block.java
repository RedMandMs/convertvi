package com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable.Block;

import com.trollfish.convertvi.IJSonSerializable;
import com.trollfish.convertvi.IJSonSerializer;
import com.trollfish.convertvi.INativeSerializable;
import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable.BlockInfoHeaderTable;

/**
 * Программное представление структуры Block
 */
public class Block implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return BlockNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return BlockJSonSerializer.getInstance();
    }

    /**
     * Конструктор блока
     * @param size - размер (в байтах)
     * @param name - имя блока
     * @param count
     * @param offset
     */
    protected Block(int size, String name, int count, int offset){
        this.size = size;
        this.name = name;
        this.count = count;
        this.offset = offset;
    }

    protected BlockInfoHeaderTable root;            //структура-родитель
    private int size;                               //размер блока (в байтах)

    protected int count;
    protected int offset;
    protected String name;

    /**
     * Гетеры и сетеры
     */
    public int getSize() {
        return size;
    }
    public String getName() {
        return name;
    }
    public int getCount() {
        return count;
    }
    public BlockInfoHeaderTable getRoot(){
        return root;
    }
    public void setRoot(BlockInfoHeaderTable root){
        this.root = root;
    }
}