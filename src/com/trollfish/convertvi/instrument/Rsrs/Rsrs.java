package com.trollfish.convertvi.instrument.Rsrs;

import com.trollfish.convertvi.IJSonSerializable;
import com.trollfish.convertvi.IJSonSerializer;
import com.trollfish.convertvi.INativeSerializable;
import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.instrument.Instrument;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable.BlockInfoDataTable;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable.BlockInfoHeaderTable;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoTableHeader.BlockInfoTableHeader;
import com.trollfish.convertvi.instrument.Rsrs.DataSetHeader.DataSetHeader;

/**
 * Программное представление структуры Rsrs
 */
public class Rsrs implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return RsrsNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return RsrsJSonSerializer.getInstance();
    }

    /**
     * Конструктор
     * @param size - размер структуры в байтах
     * @param dataSetHeader - dataSetHeader данной структуры
     * @param blockInfoTableHeader - blockInfoTableHeader данной структуры
     * @param blockInfoHeaderTable - blockInfoHeaderTable данной структуры
     * @param blockInfoDataTable - blockInfoDataTable данной структуры
     * @param byteCode - массив байтов данного ВИ-файла
     */
    protected Rsrs(int size, DataSetHeader dataSetHeader, BlockInfoTableHeader blockInfoTableHeader,
                   BlockInfoHeaderTable blockInfoHeaderTable, BlockInfoDataTable blockInfoDataTable, byte [] byteCode)
    {
        this.size = size;
        this.dataSetHeader = dataSetHeader;
        this.dataSetHeader.setRoot(this);
        this.blockInfoTableHeader = blockInfoTableHeader;
        this.blockInfoTableHeader.setRoot(this);
        this.blockInfoHeaderTable = blockInfoHeaderTable;
        this.blockInfoHeaderTable.setRoot(this);
        this.blockInfoDataTable = blockInfoDataTable;
        this.byteCode = byteCode;
        this.blockInfoDataTable.setRoot(this);
    }

    protected Instrument root;                              //структура-родитель
    protected DataSetHeader dataSetHeader;
    protected BlockInfoTableHeader blockInfoTableHeader;
    protected BlockInfoHeaderTable blockInfoHeaderTable;
    protected BlockInfoDataTable blockInfoDataTable;
    private int size;                                      //размер структуры
    private byte [] byteCode;                              //массив байтов ВИ-файла

    /**
     * Гетеры и сетеры
     */
    public int getSize() {
        return size;
    }
    public byte[] getByteCode() {
        return byteCode;
    }
    public BlockInfoDataTable getBlockInfoDataTable() {
        return blockInfoDataTable;
    }
    public Instrument getRoot(){
        return root;
    }
    public void setRoot(Instrument root){
        this.root = root;
    }

}
