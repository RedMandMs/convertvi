package com.trollfish.convertvi.instrument.Rsrs;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable.BlockInfoDataTable;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable.BlockInfoDataTableNativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable.BlockInfoHeaderTable;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoHeaderTable.BlockInfoHeaderTableNativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoTableHeader.BlockInfoTableHeader;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoTableHeader.BlockInfoTableHeaderNativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.DataSetHeader.DataSetHeader;
import com.trollfish.convertvi.instrument.Rsrs.DataSetHeader.DataSetHeaderNativeSerializer;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * vi-сериализатор Rsrs-структуры
 */
public class RsrsNativeSerializer implements INativeSerializer<Rsrs> {

    @Override
    public void serialize(Rsrs rsrs, NativeStreamWriter writer) throws IOException {

        rsrs.dataSetHeader.getNativeSerializer().serialize(rsrs.dataSetHeader, writer);
        rsrs.blockInfoTableHeader.getNativeSerializer().serialize(rsrs.blockInfoTableHeader, writer);
        rsrs.blockInfoHeaderTable.getNativeSerializer().serialize(rsrs.blockInfoHeaderTable, writer);
        rsrs.blockInfoDataTable.getNativeSerializer().serialize(rsrs.blockInfoDataTable, writer);

        ByteBuffer bb = writer.holdBytes(rsrs.getByteCode().length);
        for (int i = 0; i < rsrs.getByteCode().length; i++) {
            bb.put(rsrs.getByteCode()[i]);
        }
        writer.releaseBytes();

    }

    @Override
    public Rsrs deserialize(Object root, NativeStreamReader reader) throws IOException {
        DataSetHeaderNativeSerializer serializerDataSetHeader = DataSetHeaderNativeSerializer.getInstance();
        DataSetHeader dataSetHeader = serializerDataSetHeader.deserialize(null, reader);
        BlockInfoTableHeaderNativeSerializer serializerBlockITH = BlockInfoTableHeaderNativeSerializer.getInstance();
        BlockInfoTableHeader blockInfoTableHeader = serializerBlockITH.deserialize(null, reader);
        BlockInfoHeaderTableNativeSerializer serializerBlockIHT = BlockInfoHeaderTableNativeSerializer.getInstance();
        BlockInfoHeaderTable blockInfoHeaderTable = serializerBlockIHT.deserialize(blockInfoTableHeader, reader);
        BlockInfoDataTableNativeSerializer serializerBlockIDT = BlockInfoDataTableNativeSerializer.getInstance();
        BlockInfoDataTable blockInfoDataTable = serializerBlockIDT.deserialize(blockInfoHeaderTable, reader);

        //Вычисление размера структуры
        this.RSRS_SIZE = dataSetHeader.getSize()
                       + blockInfoHeaderTable.getSize()
                       + blockInfoTableHeader.getSize()
                       + blockInfoDataTable.getSize();

        //работа с читателем и байт-буффером
        int rest = reader.residue();
        ByteBuffer bb = reader.holdBytes(rest);
        byte [] nameFile = new byte[rest];
        for (int i = 0; i < rest; i++) {
            nameFile[i] = bb.get();
        }

        reader.releaseBytes();

        Rsrs rsrs = new Rsrs( size(), dataSetHeader, blockInfoTableHeader, blockInfoHeaderTable, blockInfoDataTable, nameFile);
        return rsrs;
    }

    @Override
    public int size() {
        System.out.println(RSRS_SIZE);
        return RSRS_SIZE;
    }

    /**
     * Метод получения экземпляра данного класса (Singleton)
     * @return - сериализатор данного класса
     */
    public static RsrsNativeSerializer getInstance() {
        if (instance == null)
            instance = new RsrsNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private RsrsNativeSerializer() {

    }

    private static int RSRS_SIZE;                           //размер (в байтах) Rsrs-структуры

    private static RsrsNativeSerializer instance = null;    //экземпляр данного класса (Singleton)
}
