package com.trollfish.convertvi.instrument.Rsrs;

import com.trollfish.convertvi.IJSonSerializer;
import org.json.simple.JSONObject;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * json-сериализатор Rsrs-структуры
 */
public class RsrsJSonSerializer implements IJSonSerializer<Rsrs> {

    @Override
    public void serialize(Rsrs rsrs, JSONObject writer) {
        rsrs.dataSetHeader.getJSonSerializer().serialize(rsrs.dataSetHeader, writer);
    }

    @Override
    public Rsrs deserialize(JSONObject reader) {
        return null;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static RsrsJSonSerializer getInstance() {
        if(instance == null)
            instance = new RsrsJSonSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private RsrsJSonSerializer() {

    }

    private static RsrsJSonSerializer instance = null;          //экземпляр данного класса (Singleton)

}