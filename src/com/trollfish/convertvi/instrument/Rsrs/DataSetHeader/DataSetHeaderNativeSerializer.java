package com.trollfish.convertvi.instrument.Rsrs.DataSetHeader;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * vi--cериализатор струтуры DataSetHeader
 */
public class DataSetHeaderNativeSerializer  implements INativeSerializer<DataSetHeader> {

    @Override
    public void serialize(DataSetHeader dataSetHeader, NativeStreamWriter writer) throws IOException {
        ByteBuffer bb = writer.holdBytes(dataSetHeader.getSize());

        bb.putInt(dataSetHeader.dataSetOffset);
        bb.putInt(dataSetHeader.dataSetSize);
        bb.putInt(dataSetHeader.dataSetId1);
        bb.putInt(dataSetHeader.dataSetId2);
        bb.putInt(dataSetHeader.dataSetId3);

        writer.releaseBytes();
    }

    @Override
    public DataSetHeader deserialize(Object addInformation, NativeStreamReader reader) throws IOException {

        ByteBuffer bb = reader.holdBytes(size());

        int dataSetOffset = bb.getInt();

        int dataSetSize = bb.getInt();

        int dataSetId1 = bb.getInt();

        int dataSetId2 = bb.getInt();

        int dataSetId3 = bb.getInt();

        DataSetHeader dataSetHeader = new DataSetHeader(
                size(), dataSetOffset, dataSetSize, dataSetId1, dataSetId2, dataSetId3
        );

        reader.releaseBytes();

        return dataSetHeader;
    }

    @Override
    public int size() {
        return DATA_SET_HEADER_SIZE; // DataSetHeader object has constant size
    }

    /**
     * Метод получения экземпляра данного класса (Singleton)
     * @return - сериализатор данного класса
     */
    public static DataSetHeaderNativeSerializer getInstance() {
        if (instance == null)
            instance = new DataSetHeaderNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private DataSetHeaderNativeSerializer() {

    }

    private static final int DATA_SET_HEADER_SIZE = 20;                 //размер (в байтах) данной структуры (всегда фиксированный)

    private static DataSetHeaderNativeSerializer instance = null;       //экземпляр данного класса (Singleton)
}
