package com.trollfish.convertvi.instrument.Rsrs.DataSetHeader;

import com.trollfish.convertvi.IJSonSerializer;
import org.json.simple.JSONObject;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * json-cериализатор струтуры DataSetHeader
 */
public class DataSetHeaderJSonSerializer implements IJSonSerializer<DataSetHeader> {

    @Override
    public void serialize(DataSetHeader object, JSONObject writer) {

    }

    @Override
    public DataSetHeader deserialize(JSONObject reader) {
        return null;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static DataSetHeaderJSonSerializer getInstance() {
        if (instance == null)
            instance = new DataSetHeaderJSonSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private DataSetHeaderJSonSerializer() {

    }

    private static DataSetHeaderJSonSerializer instance = null;         //экземпляр данного класса (Singleton)

}
