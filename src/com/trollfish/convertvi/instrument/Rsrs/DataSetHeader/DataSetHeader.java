package com.trollfish.convertvi.instrument.Rsrs.DataSetHeader;

import com.trollfish.convertvi.*;
import com.trollfish.convertvi.instrument.Rsrs.Rsrs;

/**
 * Программное представление структуры DataSetHeader
 */
public class DataSetHeader implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return DataSetHeaderNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return DataSetHeaderJSonSerializer.getInstance();
    }

    /**
     * Конструктор
     * @param size - размер данной структуры
     * @param dataSetOffset
     * @param dataSetSize
     * @param dataSetId1
     * @param dataSetId2
     * @param dataSetId3
     */
    protected DataSetHeader(int size, int dataSetOffset, int dataSetSize, int dataSetId1, int dataSetId2, int dataSetId3){
        this.size = size;
        this.dataSetOffset = dataSetOffset;
        this.dataSetSize = dataSetSize;
        this.dataSetId1 = dataSetId1;
        this.dataSetId2 = dataSetId2;
        this.dataSetId3 = dataSetId3;
    }

    protected Rsrs root;            //Родительская структурв
    protected int dataSetOffset;
    protected int dataSetSize;
    protected int dataSetId1;
    protected int dataSetId2;
    protected int dataSetId3;
    private int size;              //Размер данной структуры

    /**
     * Гетеры и сетеры
     */
    public int getSize() {
        return size;
    }
    public Rsrs getRoot(){
        return root;
    }
    public void setRoot(Rsrs root){
        this.root = root;
    }
}
