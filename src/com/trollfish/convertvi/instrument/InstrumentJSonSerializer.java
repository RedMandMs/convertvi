package com.trollfish.convertvi.instrument;

import com.trollfish.convertvi.IJSonSerializer;
import com.trollfish.convertvi.instrument.Rsrc.Rsrc;
import com.trollfish.convertvi.instrument.Rsrc.RsrcJSonSerializer;
import org.json.simple.JSONObject;

/**
 * JSON-сериализатор ВИ
 */
public class InstrumentJSonSerializer implements IJSonSerializer<Instrument> {

    @Override
    public void serialize(Instrument object, JSONObject writer) {

    }

    @Override
    public Instrument deserialize(JSONObject reader) {

        JSONObject instrumentJson= (JSONObject) reader.get("Instrument");
        System.out.println(instrumentJson);
        JSONObject globalHeader = (JSONObject) instrumentJson.get("GlobalHeader");

        RsrcJSonSerializer rsrcJSonSerializer = RsrcJSonSerializer.getInstance();
        Rsrc rsrc = rsrcJSonSerializer.deserialize(instrumentJson);

        Instrument instrument = new Instrument(globalHeader, rsrc);

        return instrument;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static InstrumentJSonSerializer getInstance() {
        if(instance == null)
            instance = new InstrumentJSonSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private InstrumentJSonSerializer() {

    }

    private static InstrumentJSonSerializer instance = null;    //экземпляр данного класса (Singleton)

}
