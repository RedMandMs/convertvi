package com.trollfish.convertvi.instrument.Rsrc;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.DataSet;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.DataSetNativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.Rsrs;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * VI-сериализатор структуры Rsrc
 */
public class RsrcNativeSerializer  implements INativeSerializer<Rsrc> {

    @Override
    public void serialize(Rsrc rsrc, NativeStreamWriter writer) throws IOException {
        ByteBuffer bb = writer.holdBytes(8); // Size Id1 and Id2

        bb.putInt(rsrc.id1);
        bb.putInt(rsrc.id2);
        writer.releaseBytes();
        rsrc.dataSet.getNativeSerializer().serialize(rsrc.dataSet, writer);
    }

    @Override
    public Rsrc deserialize(Object addInformation, NativeStreamReader reader) throws IOException {

        ByteBuffer bb = reader.holdBytes(8);      // size Id1 and Id2

        int id1 = bb.getInt();

        int id2 = bb.getInt();

        reader.releaseBytes();

        Rsrs rsrs = (Rsrs) addInformation;

        DataSetNativeSerializer serializerDataSet = DataSetNativeSerializer.getInstance();

        DataSet dataSet = serializerDataSet.deserialize(rsrs.getBlockInfoDataTable(), reader);


        this.RSRC_SIZE = 0x4 + 0x4 + dataSet.getSize();
        Rsrc rsrc = new Rsrc(
                size(), id1, id2, dataSet
        );

        reader.releaseBytes();
        return rsrc;
    }

    @Override
    public int size() {
        return RSRC_SIZE;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static RsrcNativeSerializer getInstance() {
        if (instance == null)
            instance = new RsrcNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private RsrcNativeSerializer() {

    }

    private int RSRC_SIZE = 0x8;                                    //размер (в байтах) данной структуры

    private static RsrcNativeSerializer instance = null;            //экземпляр данного класса (Singleton)
}
