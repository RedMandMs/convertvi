package com.trollfish.convertvi.instrument.Rsrc;

import com.trollfish.convertvi.*;
import com.trollfish.convertvi.instrument.Instrument;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.DataSet;

/**
 * Программное представление структуры Rsrc
 */
public class Rsrc implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return RsrcNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return RsrcJSonSerializer.getInstance();
    }

    public DataSet getDataSet() {
        return dataSet;
    }

    /**
     * Конструктор
     * @param size - размер структуры (в байтах)
     * @param id1
     * @param id2
     * @param dataSet
     */
    protected Rsrc( int size, int id1, int id2, DataSet dataSet)
    {
        this.size = size;
        this.id1 = id1;
        this.id2 = id2;
        this.dataSet = dataSet;
        this.dataSet.setRoot(this);
    }

    protected  Rsrc(DataSet dataSet){
        this.dataSet = dataSet;
    }

    protected Instrument root;                  //структура-родитель
    protected int id1;
    protected int id2;
    protected DataSet dataSet;

    private int size;                           //размер структуры (в байтах)

    /**
     * Гетеры и сетеры
     */
    @Override
    public int getSize() { return size; }
    public Instrument getRoot(){
        return root;
    }
    public void setRoot(Instrument root){
        this.root = root;
    }
}
