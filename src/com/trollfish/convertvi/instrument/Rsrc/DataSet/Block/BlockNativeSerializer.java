package com.trollfish.convertvi.instrument.Rsrc.DataSet.Block;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * VI-сериализатор структуры Block
 */
public class BlockNativeSerializer implements INativeSerializer<Block> {

    @Override
    public void serialize(Block block, NativeStreamWriter writer) throws IOException {
        ByteBuffer bb = writer.holdBytes(block.getSize());

        bb.putInt(block.getVarSize());
        for (int i = 0; i < block.data.length; i++) {
            bb.put(block.data[i]);
        }

        writer.releaseBytes();
    }

    @Override
    public Block deserialize(Object name, NativeStreamReader reader) throws IOException {
        ByteBuffer bb = reader.holdBytes(4);

        int size = bb.getInt();


        reader.releaseBytes();

        // вычисление реального размера блока
        int k = size % 4;
        if(k ==0){
            this.BLOCK_SIZE = size;
        }else{
            k = 4 - k;
            this.BLOCK_SIZE = size + k;
        }


        bb = reader.holdBytes(size());


        byte data [] = new byte[size()];
        for (int i = 0; i < data.length; i++) {
            data[i] = bb.get();
        }
        String thisName = (String) name;

        Block block = new Block(
                size()+4, size, data, thisName
        );

        reader.releaseBytes();

        return block;
    }

    @Override
    public int size() {
        return BLOCK_SIZE;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static BlockNativeSerializer getInstance() {
        if (instance == null)
            instance = new BlockNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private BlockNativeSerializer() {

    }

    private int BLOCK_SIZE;                                         //размер (в байтах) данной структуры

    private static BlockNativeSerializer instance = null;           //экземпляр данного класса (Singleton)
}