package com.trollfish.convertvi.instrument.Rsrc.DataSet.Block;

import com.trollfish.convertvi.IJSonSerializer;
import org.json.simple.JSONObject;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * Json-сериализатор структуры Block
 */
public class BlockJSonSerializer implements IJSonSerializer<Block> {

    @Override
    public void serialize(Block object, JSONObject writer) {

    }

    @Override
    public Block deserialize(JSONObject reader) {

        String information = (String) reader.get("Information");
        Block block = new Block(information);
        return block;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static BlockJSonSerializer getInstance() {
        if (instance == null)
            instance = new BlockJSonSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private BlockJSonSerializer() {

    }

    private static BlockJSonSerializer instance = null;         //экземпляр данного класса (Singleton)

}
