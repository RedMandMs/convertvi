package com.trollfish.convertvi.instrument.Rsrc.DataSet.Block;

import com.trollfish.convertvi.IJSonSerializable;
import com.trollfish.convertvi.IJSonSerializer;
import com.trollfish.convertvi.INativeSerializable;
import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.DataSet;

/**
 * Программное представление структуры Block
 */
public class Block implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return BlockNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return BlockJSonSerializer.getInstance();
    }

    /**
     * Конструктор
     * @param size - размер структуры (в байтах)
     * @param varSize
     * @param data
     * @param name
     */
    protected Block(int size, int varSize, byte data [], String name){
        this.size = size;
        this.varSize = varSize;
        this.data = data;
        this.name = name;
    }

    /**
     * Конструктор класса виртуального инструмента (customize-формат)
     * @param information - Информация о блоке
     */
    protected Block(String information){
        this.information = information;
    }

    public String getInformation() {
        return information;
    }

    private String information;
    protected DataSet root;                                 //структура-родитель
    protected byte data [];
    protected String name;
    private int varSize;
    private int size;                                       //размер структуры (в байтах)

    /**
     * Гетеры и сетеры
     */
    public int getVarSize() {
        return varSize;
    }
    public int getSize() { return size; }
    public DataSet getRoot(){
        return root;
    }
    public void setRoot(DataSet root){
        this.root = root;
    }
}
