package com.trollfish.convertvi.instrument.Rsrc.DataSet;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;
import com.trollfish.convertvi.instrument.Rsrs.BlockInfoDataTable.BlockInfoDataTable;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.Block.Block;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.Block.BlockNativeSerializer;

import java.io.IOException;
import java.util.ArrayList;

/**
 * VI-сериализатор структуры DataSet
 */
public class DataSetNativeSerializer implements INativeSerializer<DataSet> {

    @Override
    public void serialize(DataSet dataSet, NativeStreamWriter writer) throws IOException {

        for (int i=0; i<dataSet.blockList.size(); i++){
            dataSet.blockList.get(i).getNativeSerializer().serialize(dataSet.blockList.get(i), writer);
        }
    }

    @Override
    public DataSet deserialize(Object addInformation, NativeStreamReader reader) throws IOException {

        ArrayList<Block> blockList = new ArrayList<Block>();
        BlockNativeSerializer serializerBlock = BlockNativeSerializer.getInstance();

        BlockInfoDataTable blockInfoDataTable = (BlockInfoDataTable) addInformation;

        int countBlocks = blockInfoDataTable.getBlockList().size();

        this.Data_Set_Size = 0;

        for (int i=0; i<countBlocks; i++){
            String name = blockInfoDataTable.getBlockList().get(i).getName();
            Block block = serializerBlock.deserialize(name, reader);
            this.Data_Set_Size += (block.getSize() + 4);
            blockList.add(block);
        }


        DataSet newDataSet = new DataSet(
                blockList, size()
        );

        reader.releaseBytes();

        return newDataSet;
    }

    @Override
    public int size() {
        return Data_Set_Size; // ResourceHeader object has constant size
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static DataSetNativeSerializer getInstance() {
        if (instance == null)
            instance = new DataSetNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private DataSetNativeSerializer() {

    }

    private int Data_Set_Size = 0;                                  //размер (в байтах) данной структуры

    private static DataSetNativeSerializer instance = null;         //экземпляр данного класса (Singleton)
}
