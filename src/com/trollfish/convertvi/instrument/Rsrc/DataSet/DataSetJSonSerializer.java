package com.trollfish.convertvi.instrument.Rsrc.DataSet;

import com.trollfish.convertvi.IJSonSerializer;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.Block.Block;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.Block.BlockJSonSerializer;
import com.trollfish.convertvi.instrument.Rsrc.Rsrc;
import org.json.simple.JSONObject;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import java.util.ArrayList;

/**
 * Json-сериализатор структуры DataSet
 */
public class DataSetJSonSerializer implements IJSonSerializer<DataSet> {

    @Override
    public void serialize(DataSet object, JSONObject writer) {

    }

    @Override
    public DataSet deserialize(JSONObject reader) {

        JSONObject globalHeader = (JSONObject) reader.get("GlobalHeader");
        String countBlocksStr = (String) globalHeader.get("CountBlocks");
        int countBlocks = Integer.parseInt(countBlocksStr);

        JSONObject rsrcJson = (JSONObject) reader.get("Rsrc");
        JSONObject dataSetJson = (JSONObject) rsrcJson.get("DataSet");

        BlockJSonSerializer serializer = BlockJSonSerializer.getInstance();

        ArrayList<Block> blockList = new ArrayList<Block>();
        for(int i= 0; i<countBlocks; i++){
            Block block = serializer.deserialize((JSONObject) dataSetJson.get("Block "+i));
            blockList.add(block);
        }
        DataSet dataSet = new DataSet(blockList);
        return dataSet;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static DataSetJSonSerializer getInstance() {
        if (instance == null)
            instance = new DataSetJSonSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private DataSetJSonSerializer() {

    }

    private static DataSetJSonSerializer instance = null;           //экземпляр данного класса (Singleton)

}