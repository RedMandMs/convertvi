package com.trollfish.convertvi.instrument.Rsrc.DataSet;

import com.trollfish.convertvi.*;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.Block.Block;
import com.trollfish.convertvi.instrument.Rsrc.Rsrc;

import java.util.ArrayList;

/**
 * Программное представление структуры DataSet
 */
public class DataSet implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return DataSetNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return DataSetJSonSerializer.getInstance();
    }

    /**
     * Конструктор
     * @param blockList - список блоков
     * @param size - размер структуры (в байтах)
     */
    protected DataSet(ArrayList<Block> blockList, int size){
        this.blockList = blockList;
        for (int i=0; i<this.blockList.size(); i++){
            this.blockList.get(i).setRoot(this);
        }
        this.size = size;
    }

    protected  DataSet(ArrayList<Block> blockList){
        this.blockList = blockList;
    }

    protected Rsrc root;                                            //структура-родитель
    protected ArrayList<Block> blockList = new ArrayList<Block>();
    protected int size;                                             //размер структуры (в байтах)
    public ArrayList<Block> getBlockList() {
        return blockList;
    }    //список блоков

    /**
     * Гетеры и сетеры
     */
    public int getSize() {
        return size;
    }
    public Rsrc getRoot(){
        return root;
    }
    public void setRoot(Rsrc root){
        this.root = root;
    }
}