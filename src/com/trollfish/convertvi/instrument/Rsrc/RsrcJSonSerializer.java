package com.trollfish.convertvi.instrument.Rsrc;

import com.trollfish.convertvi.IJSonSerializer;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.DataSet;
import com.trollfish.convertvi.instrument.Rsrc.DataSet.DataSetJSonSerializer;
import org.json.simple.JSONObject;
import org.json.simple.JSONStreamAware;
import org.json.simple.parser.JSONParser;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * Json-сериализатор структуры Rsrc
 */
public class RsrcJSonSerializer implements IJSonSerializer<Rsrc> {

    @Override
    public void serialize(Rsrc rsrc, JSONObject writer) {
        writer.put("Id1", rsrc.id1);
        writer.put("Id2", rsrc.id2);
    }

    @Override
    public Rsrc deserialize(JSONObject reader) {
        JSONObject rsrs = (JSONObject) reader.get("Rsrs");

        DataSetJSonSerializer dataSetJSonSerializer = DataSetJSonSerializer.getInstance();
        DataSet dataSet = dataSetJSonSerializer.deserialize(reader);

        Rsrc rsrc = new Rsrc(dataSet);

        return rsrc;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static RsrcJSonSerializer getInstance() {
        if(instance == null)
            instance = new RsrcJSonSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private RsrcJSonSerializer() {

    }

    private static RsrcJSonSerializer instance = null;          //экземпляр данного класса (Singleton)

}
