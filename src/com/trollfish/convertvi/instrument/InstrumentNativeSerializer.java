package com.trollfish.convertvi.instrument;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;
import com.trollfish.convertvi.instrument.ResourceHeader.ResourceHeader;
import com.trollfish.convertvi.instrument.ResourceHeader.ResourceHeaderNativeSerializer;
import com.trollfish.convertvi.instrument.Rsrc.Rsrc;
import com.trollfish.convertvi.instrument.Rsrc.RsrcNativeSerializer;
import com.trollfish.convertvi.instrument.Rsrs.Rsrs;
import com.trollfish.convertvi.instrument.Rsrs.RsrsNativeSerializer;

import java.io.IOException;

/**
 * Сериализатор vi-формата всего ВИ
 */
public class InstrumentNativeSerializer implements INativeSerializer<Instrument> {

    @Override
    public void serialize(Instrument instrument, NativeStreamWriter writer) throws IOException {

        instrument.resourceHeaderRsrc.getNativeSerializer().serialize(instrument.resourceHeaderRsrc, writer);

        instrument.rsrc.getNativeSerializer().serialize(instrument.rsrc, writer);

        instrument.resourceHeaderRsrs.getNativeSerializer().serialize(instrument.resourceHeaderRsrs, writer);

        instrument.rsrs.getNativeSerializer().serialize(instrument.rsrs, writer);

    }

    @Override
    public Instrument deserialize(Object addInformation, NativeStreamReader reader) throws IOException {

        ResourceHeaderNativeSerializer serializerResHead = ResourceHeaderNativeSerializer.getInstance();

        ResourceHeader resourceHeaderRsrc = serializerResHead.deserialize(null, reader);

        reader.seek(resourceHeaderRsrc.getRsrc_size());

        ResourceHeader resourceHeaderRsrs = serializerResHead.deserialize(null, reader);

        RsrsNativeSerializer serializerRsrs = RsrsNativeSerializer.getInstance();

        Rsrs rsrs = serializerRsrs.deserialize(null, reader);

        reader.seek(ResourceHeaderNativeSerializer.getInstance().size());    // size ResourceHeader

        RsrcNativeSerializer serializerRsrc = RsrcNativeSerializer.getInstance();

        Rsrc rsrc = serializerRsrc.deserialize(rsrs, reader);

        this.INSTRUMENT_SIZE = resourceHeaderRsrc.getRsrc_size()+resourceHeaderRsrs.getRsrs_size();

        Instrument instrument = new Instrument(size(), resourceHeaderRsrc, resourceHeaderRsrs, rsrc, rsrs);

        return instrument;
    }

    @Override
    public int size() {
        return INSTRUMENT_SIZE;
    }

    /**
     * Метод получения экземпляра данного класса (Singleton)
     * @return - сериализатор данного класса
     */
    public static InstrumentNativeSerializer getInstance() {
        if(instance == null)
            instance = new InstrumentNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private InstrumentNativeSerializer() {

    }

    private static int INSTRUMENT_SIZE;      //размер (в байтах) виртуального инструмента

    private static InstrumentNativeSerializer instance = null;  //экземпляр данного класса (Singleton)
}
