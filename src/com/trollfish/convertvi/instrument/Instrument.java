package com.trollfish.convertvi.instrument;

import com.trollfish.convertvi.*;
import com.trollfish.convertvi.instrument.ResourceHeader.ResourceHeader;
import com.trollfish.convertvi.instrument.Rsrc.Rsrc;
import com.trollfish.convertvi.instrument.Rsrs.Rsrs;
import org.json.simple.JSONObject;


/**
 * Класс программного представления виртуального инструмента (ВИ)
 */
public class Instrument implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return InstrumentNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return InstrumentJSonSerializer.getInstance();
    }

    /**
     * Конструктор класса виртуального инструмента (native-формат)
     * @param size - размер в байтах
     * @param resourceHeaderRsrc - экземпляр класса ResourceHeader (заголовок rsrc) данного ВИ
     * @param resourceHeaderRsrs - экземпляр класса ResourceHeader (заголовок rsrs) данного ВИ
     * @param rsrc - Rsrc данного ВИ
     * @param rsrs - Rsrs данного ВИ
     */
    protected Instrument(int size, ResourceHeader resourceHeaderRsrc, ResourceHeader resourceHeaderRsrs, Rsrc rsrc, Rsrs rsrs) {
        this.size = size;
        this.resourceHeaderRsrc = resourceHeaderRsrc;
        this.resourceHeaderRsrc.setRoot(this);
        this.resourceHeaderRsrs = resourceHeaderRsrs;
        this.resourceHeaderRsrs.setRoot(this);
        this.rsrs = rsrs;
        this.rsrs.setRoot(this);
        this.rsrc = rsrc;
        this.rsrc.setRoot(this);
    }

    /**
     * Конструктор класса виртуального инструмента (customize-формат)
     * @param globalHeader - глобальный заголовок данного ВИ
     * @param rsrc - Rsrc данного ВИ
     */
    protected Instrument(JSONObject globalHeader, Rsrc rsrc){
        this.globalHeader = globalHeader;
        this.rsrc = rsrc;
        conversion();
    }

    /**
     * преобразование входных данных из json в данные для vi
     */
    private void conversion() {
        //преобразование входных данных из json в данные для vi
    }

    /**
     * расчёт выходных данных для json
     */
    private void reverseConversion() {
        //расчёт выходных данных для json
    }


    @Override
    public int getSize() {
        return this.size;
    }

    protected ResourceHeader resourceHeaderRsrc;
    protected ResourceHeader resourceHeaderRsrs;

    public Rsrc getRsrc() {
        return rsrc;
    }

    protected Rsrc rsrc;
    protected Rsrs rsrs;
    private int size;
    private  JSONObject globalHeader;

}
