package com.trollfish.convertvi.instrument.ResourceHeader;

import com.trollfish.convertvi.INativeSerializer;
import com.trollfish.convertvi.NativeStreamReader;
import com.trollfish.convertvi.NativeStreamWriter;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * VI-сериализатор структуры ResourceHeader
 */
public class ResourceHeaderNativeSerializer implements INativeSerializer<ResourceHeader> {

    @Override
    public void serialize(ResourceHeader resourceHeader, NativeStreamWriter writer) throws IOException {
        ByteBuffer bb = writer.holdBytes(resourceHeader.getSize());

        bb.put(resourceHeader.rsrc_signature.getBytes());
        bb.putShort(resourceHeader.id2);
        bb.put(resourceHeader.lvin_signature.getBytes());
        bb.put(resourceHeader.lvbw_signature.getBytes());
        bb.putInt(resourceHeader.rsrc_size);
        bb.putInt(resourceHeader.rsrs_size);

        writer.releaseBytes();
    }

    @Override
    public ResourceHeader deserialize(Object addInformation, NativeStreamReader reader) throws IOException {

        ByteBuffer bb = reader.holdBytes(size());

        byte[] rsrc_signature = new byte[6];
        bb.get(rsrc_signature);

        short id2 = bb.getShort();

        byte[] lvin_signature = new byte[4];
        bb.get(lvin_signature);

        byte[] lvbw_signature = new byte[4];
        bb.get(lvbw_signature);

        int rsrc_size = bb.getInt();

        int rsrs_size = bb.getInt();

        ResourceHeader resourceHeader = new ResourceHeader(
                new String(rsrc_signature), id2, new String(lvin_signature), new String(lvbw_signature),
                rsrc_size, rsrs_size
        );

        reader.releaseBytes();

        return resourceHeader;
    }

    @Override
    public int size() {
        return RESOURCE_HEADER_SIZE; // ResourceHeader object has constant size
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static ResourceHeaderNativeSerializer getInstance() {
        if (instance == null)
            instance = new ResourceHeaderNativeSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private ResourceHeaderNativeSerializer() {

    }

    private static final int RESOURCE_HEADER_SIZE = 0x18;               //размер (в байтах) данной структуры

    private static ResourceHeaderNativeSerializer instance = null;      //экземпляр данного класса (Singleton)
}
