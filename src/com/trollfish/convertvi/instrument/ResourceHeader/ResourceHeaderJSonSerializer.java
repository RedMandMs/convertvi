package com.trollfish.convertvi.instrument.ResourceHeader;


import com.trollfish.convertvi.IJSonSerializer;
import org.json.simple.JSONObject;

import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

/**
 * Json-сериализатор структуры ResourceHeader
 */
public class ResourceHeaderJSonSerializer implements IJSonSerializer<ResourceHeader> {

    @Override
    public void serialize(ResourceHeader resourceHeader, JSONObject writer) {
        writer.put("Id1", resourceHeader.rsrc_signature);
        writer.put("Id2", resourceHeader.id2);
        writer.put("Id3", resourceHeader.lvin_signature);
        writer.put("Id4", resourceHeader.lvbw_signature);
        writer.put("RsrcSize", resourceHeader.rsrc_size);
        writer.put("RsrsSize", resourceHeader.rsrs_size);
    }

    @Override
    public ResourceHeader deserialize(JSONObject reader) {
        String id1 = (String) reader.get("Id1");
        Short id2 = (Short) reader.get("Id2");
        String id3 = (String)reader.get("Id3");
        String id4 = (String) reader.get("Id4");
        int rsrc_size = (Integer) reader.get("RsrcSize");
        int rsrs_size = (Integer) reader.get("RsrsSize");

        ResourceHeader resourceHeader = new ResourceHeader(id1, id2, id3, id4, rsrc_size, rsrs_size);
        return resourceHeader;
    }

    /**
     * Получение экземпляра сериализатора (Singleton)
     */
    public static ResourceHeaderJSonSerializer getInstance() {
        if (instance == null)
            instance = new ResourceHeaderJSonSerializer();
        return instance;
    }

    /**
     * Пустой конструктор
     */
    private ResourceHeaderJSonSerializer() {

    }

    private static ResourceHeaderJSonSerializer instance = null;            //экземпляр данного класса (Singleton)

}
