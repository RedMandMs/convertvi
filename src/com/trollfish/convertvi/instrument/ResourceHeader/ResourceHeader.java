package com.trollfish.convertvi.instrument.ResourceHeader;

import com.trollfish.convertvi.*;
import com.trollfish.convertvi.instrument.Instrument;

/**
 * Программное представление структуры ResourceHeader
 */
public class ResourceHeader implements INativeSerializable, IJSonSerializable {

    /**
     * Получение экземпляра сериализатора vi-формата (native) (через паттерн одиночка)
     * @return - экземпляра сериализатора vi-формата
     */
    public INativeSerializer getNativeSerializer() {
        return ResourceHeaderNativeSerializer.getInstance();
    }

    /**
     * Получение экземпляра сериализатора json-формата (через паттерн одиночка)
     * @return - экземпляра сериализатора json-формата
     */
    public IJSonSerializer getJSonSerializer() {
        return ResourceHeaderJSonSerializer.getInstance();
    }

    /**
     * Конструктор
     * @param rsrc_signature
     * @param id2
     * @param lvin_signature
     * @param lvbw_signature
     * @param rsrc_size
     * @param rsrs_size
     */
    protected ResourceHeader(String rsrc_signature, short id2, String lvin_signature, String lvbw_signature,
                             int rsrc_size, int rsrs_size)
    {
        this.rsrc_signature = rsrc_signature;
        this.id2 = id2;
        this.lvin_signature = lvin_signature;
        this.lvbw_signature = lvbw_signature;
        this.rsrc_size = rsrc_size;
        this.rsrs_size = rsrs_size;
        this.size = getNativeSerializer().size();
    }

    protected Instrument root;                          //структура-родитель
    protected String rsrc_signature;
    protected short id2;
    protected String lvin_signature;
    protected String lvbw_signature;
    protected int rsrc_size;
    protected int rsrs_size;
    private int size;                                   //размер структуры (в байтах)

    /**
     * Гетеры и сетеры
     */
    public int getSize() { return size; }

    public Instrument getRoot(){
        return root;
    }

    public int getRsrc_size() {
        return rsrc_size;
    }

    public int getRsrs_size() {
        return rsrs_size;
    }

    public void setRoot(Instrument root){
        this.root = root;
    }
}