package com.trollfish.convertvi;


import org.json.simple.JSONObject;

/**
 * Интерфейс сериализатора json-формата (customize) конкретной структуры ВИ
 * @param <T> - структура для сериализации
 */
public interface IJSonSerializer<T> {

    /**
     * Сериализация структуры
     * @param object - структура ВИ
     * @param writer - писатель-JSON
     */
    public void serialize(T object, JSONObject writer);

    /**
     * Десериализация структуры
     * @param reader - читатель-JSON
     * @return - структуры в программном представлении
     */
    public T deserialize(JSONObject reader);

}
