package com.trollfish.convertvi;

/**
 * Интерфейс сериализуемой структуры для преобразования в json-формат
 */
public interface IJSonSerializable {

    /**
     * Получение объекта данного класса (Singleton)
     * @return - экземпляр класса
     */
    public IJSonSerializer getJSonSerializer();

}