package com.trollfish.convertvi;


import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


public class NativeStreamWriter {

    public NativeStreamWriter() {
        this.stream = new ByteArrayOutputStream();
        this.bytes = null;
    }

    public ByteBuffer holdBytes(int count) throws IOException {
        if(bytes != null)
            throw new IOException("bytes != null");

        bytes = new byte[count];

        return ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN);
    }

    public void releaseBytes() throws IOException {
        stream.write(bytes);
        bytes = null;
    }

    public void flushBytes(FileOutputStream outStream) throws IOException {
        outStream.write(stream.toByteArray());
        stream = new ByteArrayOutputStream();
    }

    private ByteArrayOutputStream stream;
    private byte[] bytes;
}