package com.trollfish.convertvi;

import com.trollfish.convertvi.instrument.Instrument;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by Сергей on 31.05.2014.
 * Пользовательский интерфейс программы
 */
public class GUI {

    File defaultDir= new File("C://Users/Сергей/Desktop");      //директория по-умолчанию
    File processedFile;                                         //обрабатываемый файл
    Instrument processedInstrument;                             //обрабатываемый ВИ
    JFrame mainFrame;                                           //главное окно
    JPanel jPanelwork = new JPanel();                           //главная панель
    JPanel jPanelStart = new JPanel();                          //стартовая панель
    Converter converter = new Converter();                      //Преобразователь форматов

    JMenuItem convertToVi = new JMenuItem("Convert to .vi");      //convert to .vi
    JMenuItem convertToJSON = new JMenuItem("Convert to .json");  //convert to .json

    /**
     * Создание графического пользовательского интерфейса
     */
    protected void create(){
        //создание окна, и настройка его и панелей
        mainFrame = new JFrame("VI/JSON");
        jPanelwork.setLayout(null);
        jPanelStart.setLayout(null);

        //настройка и создание MenuBar
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");               //File

        JMenuItem openVI = new JMenuItem("Open VI...");    //Open VI...
        openVI.addActionListener(new OpenVIMenuListener());
        fileMenu.add(openVI);

        JMenuItem openJSON = new JMenuItem("Open JSON...");    //Open JSON...
        openJSON.addActionListener(new OpenJSONMenuListener());
        fileMenu.add(openJSON);

        JMenu instrumentMenu = new JMenu("Instrument");               //Instrument

        //Добавление обработчиков (слушателей) для кнопок
        convertToVi.addActionListener(new ConvertToViListner());
        instrumentMenu.add(convertToVi);
        convertToVi.setEnabled(false);

        convertToJSON.addActionListener(new ConvertToJSONListner());
        instrumentMenu.add(convertToJSON);
        convertToJSON.setEnabled(false);

        menuBar.add(fileMenu);
        menuBar.add(instrumentMenu);
        mainFrame.setJMenuBar(menuBar);

        //Расположение и настройка jButtonOpenVI
        Icon iconVI = new ImageIcon(getClass().getClassLoader().getResource("res/VI.png"));
        JButton jButtonOpenVI = new JButton(iconVI);
        jButtonOpenVI.setLayout(null);
        JLabel jLabelOpenVI = new JLabel("Open VI...");
        jLabelOpenVI.setBounds(15, 100, 100, 25);
        jButtonOpenVI.add(jLabelOpenVI);
        jButtonOpenVI.setBounds(100, 160, 100, 120);
        jButtonOpenVI.addActionListener(new OpenVIMenuListener());
        jPanelStart.add(jButtonOpenVI);

        //Расположение и настройка jButtonOpenJSON
        Icon iconJSON = new ImageIcon(getClass().getClassLoader().getResource("res/JSON.png"));
        JButton jButtonOpenJSON = new JButton(iconJSON);
        jButtonOpenJSON.setLayout(null);
        JLabel jLabelOpenJSON = new JLabel("Open JSON...");
        jLabelOpenJSON.setBounds(15, 100, 100, 25);
        jButtonOpenJSON.add(jLabelOpenJSON);
        jButtonOpenJSON.setBounds(300, 160, 100, 120);
        jButtonOpenJSON.addActionListener(new OpenJSONMenuListener());
        jPanelStart.add(jButtonOpenJSON);

        //Расположение и настройка jButtonToVI
        JButton jButtonToVI = new JButton(iconVI);
        jButtonToVI.setLayout(null);
        JLabel toVI = new JLabel("Convert to VI");
        toVI.setBounds(15, 100, 100, 25);
        jButtonToVI.add(toVI);
        jButtonToVI.setBounds(330, 40, 100, 120);
        jButtonToVI.addActionListener(new ConvertToViListner());
        jPanelwork.add(jButtonToVI);

        //Расположение и настройка jButtonToJSON
        JButton jButtonToJSON = new JButton(iconJSON);
        jButtonToJSON.setLayout(null);
        JLabel toJSON = new JLabel("Convert to JSON");
        toJSON.setBounds(3, 100, 100, 25);
        jButtonToJSON.add(toJSON);
        jButtonToJSON.setBounds(330, 280, 100, 120);
        jButtonToJSON.addActionListener(new ConvertToJSONListner());
        jPanelwork.add(jButtonToJSON);

        //Расположение и настройка instrumentLebel
        Icon iconInstrument = new ImageIcon(getClass().getClassLoader().getResource("res/Instrument.png"));
        JLabel jLebelInstrument = new JLabel(iconInstrument);
        jLebelInstrument.setLayout(null);
        JLabel instrumentText = new JLabel("Instrument");
        instrumentText.setBounds(28, 100, 100, 25);
        jLebelInstrument.add(instrumentText);
        jLebelInstrument.setBounds(25, 160, 135, 120);
        jPanelwork.add(jLebelInstrument);

        //Расположение и настройка pointerUp
        Icon iconPointerUp = new ImageIcon(getClass().getClassLoader().getResource("res/PointerUp.png"));
        JLabel pointerUp = new JLabel(iconPointerUp);
        pointerUp.setBounds(165, 140, 150, 39);
        jPanelwork.add(pointerUp);

        //Расположение и настройка pointerDown
        Icon iconPointerDown = new ImageIcon(getClass().getClassLoader().getResource("res/PointerDown.png"));
        JLabel pointerDown = new JLabel(iconPointerDown);
        pointerDown.setBounds(165, 250, 150, 39);
        jPanelwork.add(pointerDown);

        //Расположение и настройка mainFrame
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.add(jPanelStart);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        mainFrame.setSize(500, 500);
        Dimension frameSize = mainFrame.getSize();
        mainFrame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
        mainFrame.setVisible(true);
    }

    /**
     * Обработчик события при нажатии на "Open VI..."
     */
    private class OpenVIMenuListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent arg0) {

            //Работа с выбором и обработкой файла
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setCurrentDirectory(defaultDir);
            jFileChooser.setAcceptAllFileFilterUsed(false);
            MyFileFilter myFileFilter = new MyFileFilter(".vi");
            jFileChooser.setFileFilter(myFileFilter);
            jFileChooser.showOpenDialog(mainFrame);

            //назначение обрабатываемого файла и обрабатываемого ВИ
            processedFile = jFileChooser.getSelectedFile();
            processedInstrument = converter.deserializeVI(processedFile);

            //разрешения работы кнопок и смена панелей
            convertToVi.setEnabled(true);
            convertToJSON.setEnabled(true);
            mainFrame.remove(jPanelStart);
            mainFrame.add(jPanelwork);
            jPanelwork.revalidate();

            //установление выбранной дирректории директоией по-умолчанию
            defaultDir = new File(processedFile.getAbsolutePath());
        }

    }

    /**
     * Обработчик события при нажатии на "Open JSON..."
     */
    private class OpenJSONMenuListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent arg0) {

            //Работа с выбором и обработкой файла
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setCurrentDirectory(defaultDir);
            jFileChooser.setAcceptAllFileFilterUsed(false);
            MyFileFilter myFileFilter = new MyFileFilter(".json");
            jFileChooser.setFileFilter(myFileFilter);
            jFileChooser.showOpenDialog(mainFrame);

            //назначение обрабатываемого файла и обрабатываемого ВИ
            processedFile = jFileChooser.getSelectedFile();
            processedInstrument = converter.deserializeJSON(processedFile);

            //разрешения работы кнопок и смена панелей
            convertToVi.setEnabled(true);
            convertToJSON.setEnabled(true);
            mainFrame.remove(jPanelStart);
            mainFrame.add(jPanelwork);
            jPanelwork.revalidate();

            //установление выбранной дирректории директоией по-умолчанию
            defaultDir = new File(processedFile.getAbsolutePath());
        }

    }

    /**
     * Обработчик события при нажатии на "Convert to VI..."
     */
    private class ConvertToViListner implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            //Создание панели выбора директории для сохранения файла
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setCurrentDirectory(defaultDir);
            jFileChooser.setDialogTitle("Save");
            jFileChooser.setApproveButtonText("Save");
            jFileChooser.setAcceptAllFileFilterUsed(false);
            jFileChooser.setFileSelectionMode(jFileChooser.DIRECTORIES_ONLY);
            MyFileFilter myFileFilter = new MyFileFilter(".vi");
            jFileChooser.setFileFilter(myFileFilter);
            jFileChooser.showOpenDialog(mainFrame);
            File dir = jFileChooser.getSelectedFile();

            //сериализация обрабатываемого ВИ
            converter.serializeToVI(dir);

            //установление выбранной дирректории директоией по-умолчанию
            defaultDir = dir;
        }

    }

    /**
     * Обработчик события при нажатии на "Convert to JSON..."
     */
    private class ConvertToJSONListner implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            //Создание панели выбора директории для сохранения файла
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setCurrentDirectory(defaultDir);
            jFileChooser.setDialogTitle("Save");
            jFileChooser.setApproveButtonText("Save");
            jFileChooser.setAcceptAllFileFilterUsed(false);
            jFileChooser.setFileSelectionMode(jFileChooser.DIRECTORIES_ONLY);
            MyFileFilter myFileFilter = new MyFileFilter(".json");
            jFileChooser.setFileFilter(myFileFilter);
            jFileChooser.showOpenDialog(mainFrame);
            File dir = jFileChooser.getSelectedFile();

            //сериализация обрабатываемого ВИ
            converter.serializeToJSON(dir);
            //установление выбранной дирректории директоией по-умолчанию
            defaultDir = dir;
        }

    }
}
